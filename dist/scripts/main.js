(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var App = function (window, undefined) {

  function toggle(trigger, container, event) {
    event.preventDefault();
    event.stopPropagation();

    if (container.classList.contains('is_active')) {
      container.classList.remove('is_active');
    } else {
      container.classList.add('is_active');
    }
  }

  function init() {
    //register dom nodes
    var searchTrigger = document.querySelector('.search-trigger'),
        siteHeader = document.querySelector('#site-header'),
        mobileNavTrigger = document.querySelector('.mobile-nav-trigger'),
        mobileNavContainer = document.querySelector('#mobile-nav');

    searchTrigger.addEventListener('click', toggle.bind(this, searchTrigger, siteHeader));
    mobileNavTrigger.addEventListener('click', toggle.bind(this, mobileNavTrigger, mobileNavContainer));
  }

  return {
    init: init
  };
}(window);

App.init();

},{}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvc2NyaXB0cy9tYWluLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7QUNBQSxJQUFJLE1BQU8sVUFBRSxNQUFGLEVBQVUsU0FBVixFQUF5Qjs7QUFFbEMsV0FBUyxNQUFULENBQWdCLE9BQWhCLEVBQXlCLFNBQXpCLEVBQW9DLEtBQXBDLEVBQTJDO0FBQ3pDLFVBQU0sY0FBTjtBQUNBLFVBQU0sZUFBTjs7QUFFQSxRQUFJLFVBQVUsU0FBVixDQUFvQixRQUFwQixDQUE2QixXQUE3QixDQUFKLEVBQStDO0FBQzdDLGdCQUFVLFNBQVYsQ0FBb0IsTUFBcEIsQ0FBMkIsV0FBM0I7QUFDRCxLQUZELE1BRU87QUFDTCxnQkFBVSxTQUFWLENBQW9CLEdBQXBCLENBQXdCLFdBQXhCO0FBQ0Q7QUFDRjs7QUFFRCxXQUFTLElBQVQsR0FBZ0I7O0FBRWQsUUFBSSxnQkFBZ0IsU0FBUyxhQUFULENBQXVCLGlCQUF2QixDQUFwQjtRQUNJLGFBQWEsU0FBUyxhQUFULENBQXVCLGNBQXZCLENBRGpCO1FBRUksbUJBQW1CLFNBQVMsYUFBVCxDQUF1QixxQkFBdkIsQ0FGdkI7UUFHSSxxQkFBcUIsU0FBUyxhQUFULENBQXVCLGFBQXZCLENBSHpCOztBQUtBLGtCQUFjLGdCQUFkLENBQStCLE9BQS9CLEVBQXdDLE9BQU8sSUFBUCxDQUFZLElBQVosRUFBa0IsYUFBbEIsRUFBaUMsVUFBakMsQ0FBeEM7QUFDQSxxQkFBaUIsZ0JBQWpCLENBQWtDLE9BQWxDLEVBQTJDLE9BQU8sSUFBUCxDQUFZLElBQVosRUFBa0IsZ0JBQWxCLEVBQW9DLGtCQUFwQyxDQUEzQztBQUNEOztBQUVELFNBQU87QUFDTCxVQUFPO0FBREYsR0FBUDtBQUlELENBNUJTLENBNEJMLE1BNUJLLENBQVY7O0FBOEJBLElBQUksSUFBSiIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCJ2YXIgQXBwID0gKCggd2luZG93LCB1bmRlZmluZWQgKSA9PiB7XG5cbiAgZnVuY3Rpb24gdG9nZ2xlKHRyaWdnZXIsIGNvbnRhaW5lciwgZXZlbnQpIHtcbiAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuICBcbiAgICBpZiAoY29udGFpbmVyLmNsYXNzTGlzdC5jb250YWlucygnaXNfYWN0aXZlJykpIHtcbiAgICAgIGNvbnRhaW5lci5jbGFzc0xpc3QucmVtb3ZlKCdpc19hY3RpdmUnKVxuICAgIH0gZWxzZSB7XG4gICAgICBjb250YWluZXIuY2xhc3NMaXN0LmFkZCgnaXNfYWN0aXZlJylcbiAgICB9XG4gIH1cblxuICBmdW5jdGlvbiBpbml0KCkge1xuICAgIC8vcmVnaXN0ZXIgZG9tIG5vZGVzXG4gICAgbGV0IHNlYXJjaFRyaWdnZXIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuc2VhcmNoLXRyaWdnZXInKSxcbiAgICAgICAgc2l0ZUhlYWRlciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNzaXRlLWhlYWRlcicpLFxuICAgICAgICBtb2JpbGVOYXZUcmlnZ2VyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLm1vYmlsZS1uYXYtdHJpZ2dlcicpLFxuICAgICAgICBtb2JpbGVOYXZDb250YWluZXIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjbW9iaWxlLW5hdicpO1xuXG4gICAgc2VhcmNoVHJpZ2dlci5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIHRvZ2dsZS5iaW5kKHRoaXMsIHNlYXJjaFRyaWdnZXIsIHNpdGVIZWFkZXIpKTtcbiAgICBtb2JpbGVOYXZUcmlnZ2VyLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgdG9nZ2xlLmJpbmQodGhpcywgbW9iaWxlTmF2VHJpZ2dlciwgbW9iaWxlTmF2Q29udGFpbmVyKSk7XG4gIH1cbiAgXG4gIHJldHVybiB7XG4gICAgaW5pdCA6IGluaXQsXG4gIH07XG4gIFxufSApKCB3aW5kb3cgKTtcblxuQXBwLmluaXQoKTsiXX0=
