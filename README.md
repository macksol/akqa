##Name:
Mack Solomon


##Setup Instructions:

Clone the repo:

`git clone git@bitbucket.org:macksol/akqa.git`

Install npm dependencies:

`npm install`

Run Gulp:

`gulp`


##Tests:
```
# markup
gulp testMarkup

# PageSpeed Insight report
gulp testPsi

```


##Tools used:
* Gulp 
* ESLint with airbnb config
* Babel ES6 transpiler
* Autoprefixer
* SCSS
* normalize.css
* ImageMin


##Editor used:
Sublime Text 3


##Time taken:
~5 hours


##Additional comments (optional):
* Per the requirements I am not using a frameowrk like Bootstrap but I am using a

small custom grid system that I devised from scratch.

* I decided to use Gulp as a build system. It's been my experience that 

when projects increase in size and complexity Grunt can be pretty sluggish.

Gulp has always been pretty snappy for me.
