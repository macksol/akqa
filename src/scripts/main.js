var App = (( window, undefined ) => {

  function toggle(trigger, container, event) {
    event.preventDefault();
    event.stopPropagation();
  
    if (container.classList.contains('is_active')) {
      container.classList.remove('is_active')
    } else {
      container.classList.add('is_active')
    }
  }

  function init() {
    //register dom nodes
    let searchTrigger = document.querySelector('.search-trigger'),
        siteHeader = document.querySelector('#site-header'),
        mobileNavTrigger = document.querySelector('.mobile-nav-trigger'),
        mobileNavContainer = document.querySelector('#mobile-nav');

    searchTrigger.addEventListener('click', toggle.bind(this, searchTrigger, siteHeader));
    mobileNavTrigger.addEventListener('click', toggle.bind(this, mobileNavTrigger, mobileNavContainer));
  }
  
  return {
    init : init,
  };
  
} )( window );

App.init();